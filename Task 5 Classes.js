
class ShoppingCart {
    constructor(quantity, dateAdded) {
        this._cartID = 0,
            this._productID = 0,
            this.quantity = quantity,
            this.dateAdded = dateAdded
    }

    set cartID(val) {
        if (typeof val !== "number") {
            throw new Error("Invalid type");
        }
        this._cartID = val;
    }
    get cartID() {
        return this._cartID;
    }

    set productID(val) {
        if (typeof val !== "number") {
            throw new Error("Invalid type");
        }
        this._productID = val;
    }
    get cartID() {
        return this._productID;
    }

    addCartItem() {
        console.log(`Item added`)
    }

    updateQantity() {
        console.log(`Qantity updated`)
    }

    viewCartDetails() {
        console.log(`Cart details viewed`)
    }

    checkOut() {
        console.log(`Cart checked`)
    }
}

const shoppingCart = new ShoppingCart();

try {
    shoppingCart.cartId = 1;
    shoppingCart.productId = 1;
} catch (error) {
    console.log(error.message);
}

//========================================
// 
//========================================

class ShippingInfo {
    constructor() {
        this._shippingID = "",
        this._shippingType = "",
        this._shippingCost = "",
        this._shippingRegionID = ""
    }

    set shippingId(val){
        if(typeof val !== "number"){
            throw new Error('Invalid type')
        }
        this._shippingId = val;
    }
    get shippingId(){
        return this._shippingId;
    }

    set shippingType(val){
        if(typeof val !== "string"){
            throw new Error('Invalid type')
        }
        this._shippingType = val;
    }
    get shippingType(){
        return this._shippingType;
    }

    set shippingCost(val){
        if(typeof val !== "number"){
            throw new Error('Invalid type')
        }
        this._shippingCost = val;
    }
    get shippingCost(){
        return this._shippingCost;
    }

    set shippingRegionId(val){
        if(typeof val !== "number"){
            throw new Error('Invalid type')
        }
        this._shippingRegionId = val;
    }
    get shippingRegionId(){
        return this._shippingRegionId;
    }

    updateShippingInfo() {
        console.log("Shipping info updated")
    }
}

const shippingInfo = new ShippingInfo();

try {
    shippingInfo.shippingId = 'abc';
    shippingInfo.shippingType = 1;
    shippingInfo.shippingCost = 1;
    shippingInfo.shippingRegionId = 1;
} catch (error) {
    console.log(error.message);
}

//========================================
// 
//========================================

class OrderDetails {
    constructor(productName, quantity, unitCost, subtotal) {
        this._orderID = 0,
        this._productID = 0,
        this.productName = productName,
        this.quantity = quantity,
        this.unitCost = unitCost,
        this.subtotal = subtotal
    }

    set orderId(val){
        if(typeof val !== "number"){
            throw new Error('Invalid type')
        }
        this._orderId = val;
    }
    get orderId(){
        return this._orderId;
    }

    set productId(val){
        if(typeof val !== "number"){
            throw new Error('Invalid type')
        }
        this._productId = val;
    }
    get productId(){
        return this._productId;
    }

    calcPrice() {
        console.log("Price calculated")
    }
}

const orderDetails = new OrderDetails();

try {
    orderDetails.orderId = 1;
    orderDetails.productId = 1;
} catch (error) {
    console.log(error.message);
}

//========================================
// 
//========================================

class Administrator {
    constructor(loginStatus) {
        this.adminName = loginStatus,
        this.email = ""
    }

    set email(val){
        if(typeof val !== "string"){
            throw new Error('Invalid type')
        }
        this._email = val;
    }
    get email(){
        return this._email;
    }

    updateCatalog() { }
}

const administrator = new Administrator();

try {
    administrator.cartId = "a@gmail.com";
} catch (error) {
    console.log(error.message);
}

//========================================
// 
//========================================

class Order {
    constructor(dateShiped, customerName, customerID, status, shippingID) {
        this.orderID = "",
        this.dateCreated = "",
        this.dateShiped = dateShiped,
        this.customerName = customerName,
        this.customerID = customerID,
        this.status = status,
        this.shippingID = shippingID
    }

    set orderId(val){
        if(typeof val !== "number"){
            throw new Error('Invalid type')
        }
        this._orderId = val;
    }
    get orderId(){
        return this._orderId;
    }

    set dateCreated(val){
        if(typeof val !== "string"){
            throw new Error('Invalid type')
        }
        this._dateCreated = val;
    }
    get dateCreated(){
        return this._dateCreated;
    }

    placeOrder() {
        console.log("Order placed")
    }
}

const order = new Order();

try {
    order.dateShiped = "abc",
    order.orderId = 1;
    order.dateCreated = 'abc';
    order.shippingId = 'abc';
} catch (error) {
    console.log(error.message);
}

//========================================
// 
//========================================

class Customer {
    constructor(customerName) {
        this.customerName = customerName,
        this.addres = "",
        this.email = "",
        this.creditCardInfo = "",
        this.shippingInfo = "",
        this.accountBalance = ""
    }

    set address(val){
        if(typeof val !== "string"){
            throw new Error('Invalid type')
        }
        this._address = val;
    }
    get address(){
        return this._address;
    }

    set email(val){
        if(typeof val !== "string"){
            throw new Error('Invalid type')
        }
        this._email = val;
    }
    get email(){
        return this._email;
    }

    set creditCardInfo(val){
        if(typeof val !== "string"){
            throw new Error('Invalid type')
        }
        this._creditCardInfo = val;
    }
    get creditCardInfo(){
        return this._creditCardInfo;
    }

    set shippingInfo(val){
        if(typeof val !== "string"){
            throw new Error('Invalid type')
        }
        this._shippingInfo = val;
    }
    get shippingInfo(){
        return this._shippingInfo;
    }

    set accountBalance(val){
        if(typeof val !== "number"){
            throw new Error('Invalid type')
        }
        this._accountBalance = val;
    }
    get accountBalance(){
        return this._accountBalance;
    }

    register() {
        console.log("Registration")
    }
    login() {
        console.log("Login")
    }
    updateProfile() {
        console.log("Profile updated")
    }
}

const customer = new Customer();

try {
    customer.address = 'abc';
    customer.email = 'a@gmail.com';
    customer.creditCardInfo = 'abc';
    customer.shippingInfo = 'abc';
    customer.accountBalance = 1;
} catch (error) {
    console.log(error.message);
}

//========================================
//
//========================================

class User {
    constructor(loginStatus, registerDate) {
        this.userID = "",
        this.password = "",
        this.loginStatus = loginStatus,
        this.registerDate = registerDate
    }

    set userId(val){
        if(typeof val !== "string"){
            throw new Error('Invalid type')
        }
        this._userId = val;
    }
    get userId(){
        return this._userId;
    }
    
    set password(val){
        if(typeof val !== "string"){
            throw new Error('Invalid type')
        }
        this._password = val;
    }
    get password(){
        return this._password;
    }

    verifyLogin() { }
}

const user = new User;

try {
    user.userId = 'abc';
    user.password = 'abc';
} catch (error) {
    console.log(error.message);
}